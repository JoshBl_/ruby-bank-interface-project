#class defined
class Account
    #read the name and balance variables
    #to use the reader, the attributes need to be symbols!
    attr_reader :name, :balance, :pin
    #method to set the values for name and balance
    #balance is an optional variable, if balance isn't given a value, then 100 will be provided
    def initialize (name, balance=100, pin)
        @name = name
        @balance = balance
        @pin = pin
    end

    #stating public method
    public
    def display_balance (pin_number)
        #ternary expression
        puts pin_number == pin ? "Balance $#{balance}." : pin_error
    end

    #public method
    public
    def withdraw (pin_number, amount)
        #checking if the supplied pin number is correct
        if pin_number == pin
            #perform maths
            @balance -= amount
            #display new amount and withdrawn amount
            puts "Withdrew #{amount}, balance is $#{@balance}"
        else
            pin_error
        end
    end

    #public method
    public
    def deposit (pin_number, amount)
        if pin_number == pin
            #perform maths
            @balance += amount
            #display new amount and deposit amount
            puts "Deposited #{amount}, balance is $#{@balance}"
        else
            pin_error
        end
    end

    #private method
    private
    def pin_error
        return "Access denied: Incorrect pin"
    end
end

puts "Hello and welcome to Josh Bank in Ruby!"
puts "Please enter your username!"

#get user input and assign it to username variable
username = gets.chomp

puts "Pleased to meet you - #{username}"
puts "Now - enter your starting balance"

#get user input and assign it to startingBalance variable
#stored as an integer
startingBalance = gets.chomp.to_i
puts "Your starting balance will be #{startingBalance}"

#get user input and assign it to userPin
#stored as an integer
puts "Enter your PIN"
userPin = gets.chomp.to_i
puts "Your PIN will be #{userPin}"

puts "Lets create your account!"

#creating test object of the Account class
test_account = Account.new(username, startingBalance, userPin)

puts "Account created!"

#creating a new boolean variable, setting to true by default
check = true

#while loop to ensure that the user stays within the menu unless the user quits
while check == true
    puts "Welcome to the main menu. Enter a command!"
    puts"(D)isplay balance"
    puts "(W)ithdraw balance"
    puts "De(P)osit"
    puts "(Q)uit"
    choice = gets.chomp

    #case statement used to create multiple choice menu
    case choice
    when 'D'
        #stored as an integer
        puts "Enter your pin to view your balance!"
        pinInput = gets.chomp.to_i
        puts test_account.display_balance(pinInput)
    when 'W'
        puts "Enter your pin!"
        pinInput = gets.chomp.to_i
        puts "Enter how much you want to withdraw"
        #stored as an integer
        withdrawAmount = gets.chomp.to_i
        puts test_account.withdraw(pinInput, withdrawAmount)
    when 'P'
        puts "Enter your pin!"
        pinInput = gets.chomp.to_i
        puts "Enter how much you want to deposit!"
        #stored as an integer
        depositInput = gets.chomp.to_i
        puts test_account.deposit(pinInput, depositInput)
    when 'Q'
        check = false
        puts "Bye!"
    #else for catching errors
    else
        puts "Error!"
    end
end